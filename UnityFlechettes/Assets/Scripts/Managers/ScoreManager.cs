using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    private GameObject monitor;

    [SerializeField]
    private GameObject restartButton;

    [SerializeField]
    private GameObject[] darts;

    private Monitor _monitor;

    private int score = 0;
    private int nbThrown = 0;

    void Start()
    {
        Debug.Log("Score : " + score + "pts");
        string[] lines = { "Score", "0pts", "", "Lancers restants : 3", "" };
        _monitor = monitor.GetComponent<Monitor>();
        if (_monitor != null) _monitor.UpdateCanvas(lines);
    }

    public void IncrementScore(int value)
    {
        score += value;
        nbThrown++;
        Debug.Log("Score : " + score + "pts (+" + value + "pts)");
        string[] lines = { "Score", score + "pts (+" + value + "pts)", "", "Lancers restants : " + (3 - nbThrown), "" };
        if (_monitor != null) _monitor.UpdateCanvas(lines);

        

        nbThrown++;
        if (nbThrown >= 3)
        {
            // Fin de partie
            // Enlever flechettes de la cible
            // Reset position des flechettes
            
            // Reset score
            // Ecran de fin => Rejouer
            string[] linesEnd = { "Score", "", score + "pts (+" + value + "pts)", "", "Partie termin�e !" };
            if (_monitor != null) _monitor.UpdateCanvas(linesEnd);

            //restartButton.SetActive(true);
        }
    }

    public void RestartGame()
    {
        Dart dart;
        foreach (GameObject obj in darts)
        {
            dart = obj.GetComponent<Dart>();
            dart.Reset();
        }

        score = 0;
        nbThrown = 0;
        string[] lines = { "Score", "0pts", "", "Lancers restants : 3", "" };
        _monitor = monitor.GetComponent<Monitor>();
        if (_monitor != null) _monitor.UpdateCanvas(lines);
    }
}
