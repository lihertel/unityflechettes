using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    private static int[] _scoreMapping = { 3, 19, 7, 16, 8, 11, 14, 9, 12, 5, 20, 1, 18, 4, 13, 6, 10, 15, 2, 17 };
    private static float ZONE_WIDTH = 2 * Mathf.PI / 20; // 20 zones sur la cible

    private ScoreManager _scoreManager;
    private float _boxColliderSize;

    void Start()
    {
        _boxColliderSize = GetComponent<BoxCollider>().size.x;
        _scoreManager = FindObjectOfType<ScoreManager>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Récupération du point de collision dans le repère monde
        Vector3 position3DInWorldCoord = collision.contacts[0].point;

        // Passage du repère monde au repère de la cible (2D)
        Vector3 position3DInTarget = transform.InverseTransformPoint(position3DInWorldCoord) / _boxColliderSize;

        // Calcul de l'angle et de la distance au centre
        Vector2 position2DInTarget = new Vector2(-position3DInTarget.x, -position3DInTarget.z) * 2;
        float angle = Mathf.Atan2(position2DInTarget.x, position2DInTarget.y) + Mathf.PI + ZONE_WIDTH / 2;
        float distance = Vector2.Distance(Vector2.zero, position2DInTarget);

        // Calcul des points à partir de l'angel et de la distance
        int score = ComputeScore(angle, distance);
        _scoreManager.IncrementScore(score);

        GameObject obj = collision.GetContact(0).otherCollider.gameObject;
        Dart dart = obj.GetComponent<Dart>();
        if(dart != null) dart.DisablePhysics();
    }

    private int ComputeScore(float angle, float distance)
    {
        int index = (int)(angle / ZONE_WIDTH);
        if (index == 20) index = 0; // Zone 20 = Zone 0

        if (distance > 0 && distance < 0.040f) return 50;
        else if (distance > 0.040f && distance < 0.0968f) return 25;
        else return _scoreMapping[index] * BonusMultiplier(distance);
    }

    private int BonusMultiplier(float distance)
    {
        if (distance > 0.946f && distance < 1) return 2;
        else if (distance > 0.577f && distance < 0.631f) return 3;
        else return 1;
    }
}

// Calcul des points :

// 0 0.040          50pts
// 0.040 0.0968     25pts

// 0.577 0.631      x3
// 0.946 1          x2