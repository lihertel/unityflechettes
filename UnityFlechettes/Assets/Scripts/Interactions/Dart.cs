using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dart : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private Vector3 _originalPosition;
    private Quaternion _originalRotation;

    private GameObject _grabAttach = null;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _originalPosition = gameObject.transform.position;
        _originalRotation = gameObject.transform.localRotation;
        _grabAttach = GameObject.Find("[RightHand Controller] Attach");
        if (_grabAttach != null) _grabAttach.transform.localRotation = Quaternion.Euler(new Vector3(-45f, 0, 0));
    }

    public void Reset()
    {
        Debug.Log("Enabled");
        
        _rigidbody.isKinematic = false;
        _rigidbody.detectCollisions = true;

        gameObject.transform.position = _originalPosition;
        gameObject.transform.localRotation = _originalRotation;
    }

    public void DisablePhysics()
    {
        Debug.Log("Disabled");
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.isKinematic = true;
        _rigidbody.detectCollisions = false;
        transform.localRotation = Quaternion.Euler(new Vector3(90f, -80, -90f));
    }
}
