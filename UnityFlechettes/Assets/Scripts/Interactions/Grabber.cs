using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using Unity.XR.CoreUtils;

public class Grabber : MonoBehaviour
{
    public XRNode controller;
    private InputDevice rightController;
    private RaycastHit hit;
    private GameObject grabbedObj = null;
    private bool attrape = false;

    void Update()
    {
        rightController = InputDevices.GetDeviceAtXRNode(controller);
        rightController.TryGetFeatureValue(CommonUsages.triggerButton, out bool triggerValue);

        if (triggerValue)    // si l'utilisateur appuie sur la gachette
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit, 10.0f))    // si l'on vise un objet et qu'il est proche
            {
                if (hit.collider.gameObject.tag == "Dart" && !attrape)     // et que cet objet est la boule
                {
                    attrape = true;
                    grabbedObj = hit.collider.gameObject;    // on l'attrape
                    grabbedObj.transform.SetParent(transform);
                    grabbedObj.transform.localPosition = new Vector3(0f, 0f, 0f);
                    //grabbedObj.GetComponent<Rigidbody>().isKinematic = true;

                }
            }
        }
        // sinon (si l'on appuie pas sur la gachette)
        else
        {
            if (attrape)
            {
                grabbedObj.transform.SetParent(null); // si l'utilisateur n'appuie pas sur la gachette mais qu'on avait attrap� un objet, on le lache
                attrape = false;
                //grabbedObj.GetComponent<Rigidbody>().isKinematic = false;
            }
        }
    }
}

