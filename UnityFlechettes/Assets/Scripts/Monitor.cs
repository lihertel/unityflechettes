using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Monitor : MonoBehaviour
{
    private Text[] _lines;

    private void Start()
    {
        _lines = transform.GetComponentsInChildren<Text>();
    }

    public void UpdateCanvas(string[] lines)
    {
        for (int i = 1; i < _lines.Length; i++)
        {
            _lines[i].text = lines[i];
        }
    }
}
